# Bayesian Tensorflow

This is the code from the paper "*Training and Compression of Bayesian Neural Networks through Free Energy Minization*". It contains a python package and demos of the experiments performed in the paper.

The source code for the package is contained in the `src` folder. It can be installed by running
```
py -m pip install .
```
from this directory of the repo. The demos are all contained in the `demos` folder, this also includes the relevant datasets.